<?php
  
  ## php HTTP client
  ## http://phphttpclient.com/
  include('httpful.phar');

  class DijjitSDK
  {

    private $api_username   = null;
    private $api_password   = null;
    private $api_token      = null;
    private $api_login_url  = "http://dijjit.com/api/APILogin/Login";

    #
    # Constructor function. (set us up the bomb)
    function __construct($api_username = null, $api_password = null) 
    {
      #
      # Pass the construct vars to the class.
      $this->api_username = $api_username;
      $this->api_password = $api_password;

      #
      # Error out if not passed
      if ( $this->api_username == null || $this->api_password == null )
      {
        throw new Exception('API username on construct is required: $foo = new DijjitSDK(USERNAME,PASSWORD);');
      }

      #
      # Checks if token is set, sets token if false.
      if ( !$this->chk_token() ) { $this->get_token(); }
    }

    #
    # utility, what did I pass in here?!
    function show_vars() {
      echo "Username: ". $this->api_username;
      echo "\n";
      echo "Password: ".$this->api_password;
      echo "\n";
      echo "Token: ".$this->api_token;
      echo "\n";
      echo "\n";
    }

    #
    # Gives a true or false value if the API Token is set or not. 
    function chk_token() 
    {
      if ($this->api_token === null) { return false; } else { return true; }
    }

    #
    # Makes the Request to login and sets the API token.
    function get_token()
    {

      # Setup and send the POST request to the API.
      $response = \Httpful\Request::post($this->api_login_url)
        ->addHeaders(
          array(
            "Content-Type" => "application/x-www-form-urlencoded"
          )
        )
        ->body("Username=".urlencode($this->api_username)."&Password=".urlencode($this->api_password))
        ->send();

      #
      # Check for an errd reponse
      if ( isset($response->body->errorMessage) == true )
      {
        # Throw err.
        throw new Exception($response->body->errorMessage);
      } 
      else 
      {
        #
        # if the response has no error, set our class token.
        $this->api_token = $response->body->Token;
      }
    }

  #
  } # End Class
