# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # 
  # Default user is ubuntu rather than vagrant
  config.ssh.username = "vagrant"

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty"
  config.vm.define "development.dijjit_php_sdk"

  #
  # Provisioners
  config.vm.provision :shell, path: "./bin/install.php.sh",       privileged: true

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  #config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 8080,  host: 8080    # web
  #config.vm.network "forwarded_port", guest: 15672, host: 15672   # rabbitmq - Docker if needed
  #config.vm.network "forwarded_port", guest: 3306,  host: 3306    # mysql - Docker if needed


  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "10.0.0.2"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "./", "/home/ubuntu/dijjit_php_sdk"
  config.vm.synced_folder "./", "/app/dijjit_php_sdk"

  #
  #
  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # View the documentation for the provider you are using for more
  # information on available options.
  config.vm.provider :virtualbox do |v|
    v.name = "development.dijjit_php_sdk"

    v.memory = 2048 # 2GB
    v.cpus = 1 # 1 cores.
  end

  #
  # Triggers the start of MYSQL and RabbitMQ Docker Containers
  config.trigger.after :up, :append_to_path => "/usr/bin" do
    # run_remote "docker run -d -v /mnt/sda1/var/mysql_data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=supersecret -it -p 3306:3306 mysql" # Uncomment to have a local MYSQL server
    # run_remote "docker run -d  -p 15672:15672 -p 5672:5672  rabbitmq:3.5.2-management" # Uncomment for a local queue service
  end
end
